#FormBuilder wrapper for meteor

This is a simple wrapper around the js library [FormBuilder](https://formbuilder.online/)

#Usage

This module wrap formBuilder in a template `formBuilder`.
It accepts one reactive varriable `formUid=` to specify the name of a pre-existing form.

The collection holding all forms can be subscribed as `FormBuilder.dynamicForms` and accessed using the public api as `FormBuilder.Collections.DynamicForms`

#Example

```
<template name="main">
  {{#if Template.subscriptionsReady}}
    {{> formBuilder formUid=formUid}}
    <ul>
      {{#each form in forms}}
        <li><a href="#" data-action="form-edit" data-uid="{{form.name}}">{{form.name}}</a></li>
      {{/each}}
    </ul>
  {{/if}}
</template>
```

```
Template.main.onCreated(function(){
  this.subscribe("FormBuilder.dynamicForms");
  this.formUid = new ReactiveVar();
});

Template.main.helpers({
  'forms': function(){
    return FormBuilder.Collections.DynamicForms.find().fetch();
  },
  'formUid': function() {
    return Template.instance().formUid;
  }
});

Template.main.events({
  'click [data-action="form-edit"]': function(event,template) {
    var formId = $(event.target).data('uid');
    template.formUid.set(formId);
  }
});
```

#API

- `FormBuilder.toSimpleSchema(form)`
   Given a the form data (as provided by the formBuilder.js library), return the equivalent SimpleSchema Object.

- `FormBuilder.attachFormBuilderSchema(schema, formname)`
   Given a schema and the form name (unique machine readable identifier) return the new schema object (use node-simpl-schema `extend` functionality).

#Dependencies
- jquery-ui
- jquery-ui-sortable

And SimpleSchema that is a npm peer dependency

```
meteor npm install --save simpl-schema
```
