import SimpleSchema from 'simpl-schema'
import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions'

checkNpmVersions({ 'simpl-schema': '0.3.x' }, 'abate:formbuilder')

SimpleSchema.extendOptions(['placeholder', 'instructions', 'autoform'])

function SSType(e) {
  const obj = {
    type: e.jsType,
    label: e.label,
    instructions: e.description || '',
    placeholder: e.placeholder || '',
    optional: (!(typeof e.required !== 'undefined')),
  }
  if (Package['aldeed:autoform']) {
    obj.autoform = e.hasOwnProperty('autoform') ? _.clone(e.autoform) : {}
    if (e.hasOwnProperty('group')) {
      obj.autoform.group = e.group
    }
    if (e.hasOwnProperty('groupHelp')) {
      obj.autoform.groupHelp = e.groupHelp
    }
    if (e.hasOwnProperty('description')) {
      obj.autoform.afFieldHelpText = e.description
    }
  }
  if (e.realtype) {
    // this is necessary for SimpleSchema 0.3
    return ({ [e.name]: obj, [`${e.name}.$`]: e.realtype })
  }
  return ({ [e.name]: obj })
}

export const toSimpleSchema = function toSimpleSchema(formData) {
  const res = {}
  if (formData) {
    formData.form.forEach((elem) => {
      // if (Package['aldeed:autoform']) { elem.autoform = {}; }
      switch (elem.type) {
        case 'number':
          elem.jsType = Number
          break
        case 'date':
          elem.jsType = Date
          if (Package['abate:autoform-datetimepicker']) {
            elem.autoform = {
              type: 'flatpicker',
              opts: { altInput: true, altFormat: 'F j, Y' },
            }
          }
          break
        case 'checkbox':
          elem.jsType = Boolean
          break
        case 'select':
          elem.jsType = ((elem.multiple === true) ? Array : String)
          if (elem.multiple === true) { elem.realtype = String }
          if (Package['aldeed:autoform']) {
            elem.autoform = { type: 'select' }
            if (Object.prototype.toString.call(elem.values) === '[object Array]') {
              elem.autoform.options = function() { return elem.values }
            }
          }
          break
        case 'checkbox-group':
          elem.jsType = Array
          elem.realtype = String
          if (Package['aldeed:autoform']) {
            elem.autoform = { type: 'select-checkbox' }
            elem.autoform.options = function() { return elem.values }
          }
          break
        case 'radio-group':
          elem.jsType = String
          if (Package['aldeed:autoform']) {
            elem.autoform = { type: 'select-radio' }
            elem.autoform.options = function() { return elem.values }
          }
          break
        case 'textarea':
          elem.jsType = String
          if (Package['aldeed:autoform']) {
            elem.autoform = { type: 'textarea' }
            if (elem.rows) {
              elem.autoform.rows = elem.rows
            } else {
              elem.autoform.rows = 5
            }
          }
          break
        case 'file':
          elem.jsType = String
          if (Package['aldeed:autoform']) {
            elem.autoform = { type: 'fileUpload' }
          }
          break
        default:
          elem.jsType = String
      }
      _.extend(res, SSType(elem))
    })
    return new SimpleSchema(res)
  }
}

export const attachFormBuilderSchema = function attachFormBuilderSchema(schema, formname) {
  if (Meteor.isClient) {
    Meteor.subscribe('FormBuilder.dynamicForms', () => {
      const form = DynamicForms.findOne({ name: formname })
      if (form) {
        const ss = toSimpleSchema(form)
        schema.extend(ss)
      } else {
        console.log(`${formname} Does not exists`)
      }
    })
  }
}
