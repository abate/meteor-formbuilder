import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions'

import SimpleSchema from 'simpl-schema'

checkNpmVersions({ 'simpl-schema': '0.3.x' }, 'abate:formbuilder')

Schemas = {}

// this collection holds all the dynamicForms.
DynamicForms = new Mongo.Collection('FormBuilder.dynamicForms')

// a DynamicForms is identified by a name and contains the json object
// defining the form itself.
Schemas.DynamicForms = new SimpleSchema({
  name: { type: String, optional: false },
  form: { type: Object, blackbox: true },
})
