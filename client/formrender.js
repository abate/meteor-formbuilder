import { toSimpleSchema } from '../both/helpers'

ReactiveForms.createFormBlock({
  template: 'basicFormBuilderBlock',
  submitType: 'normal',
})

ReactiveForms.createElement({
  template: 'basicFormBuilderSelect',
  validationEvent: 'keyup',
  reset(el) {
    $(el).val('')
  },
})

ReactiveForms.createElement({
  template: 'basicFormBuilderTextarea',
  validationEvent: 'keyup',
  reset(el) {
    $(el).val('')
  },
})

ReactiveForms.createElement({
  template: 'basicFormBuilderInput',
  validationEvent: 'keyup',
  reset(el) {
    $(el).val('')
  },
})

ReactiveForms.createElement({
  template: 'basicFormBuilderInputGroup',
  validationEvent: 'keyup',
  reset(el) {
    $(el).val('')
  },
})

SSTypes = ['String', 'Number', 'Boolean', 'Object', 'Date']

Template.basicFormBuilderInputGroup.helpers({
  checked(t) { if (t) { return 'checked' } return null },
  realType(t) {
    switch (t) {
      case 'radio-group': return 'radio'
      case 'checkbox-group': return 'checkbox'
      default:
    }
    return null
  },
})

Template.basicFormBuilderSelect.helpers({
  selected(t) { if (t) { return 'selected' } },
})

Template.basicFormBuilderInput.helpers({
  checked(t) { if (t) { return 'checked' } },
})

Template.formBuilderRender.onCreated(() => {
  this.subscription = this.subscribe('FormBuilder.dynamicForms')
})

Template.formBuilderRender.helpers({
  equal(a, b) { return (a === b) },
  schema() {
    const { name } = Template.currentData()
    const data = DynamicForms.findOne({ name }).form
    if (data) {
      return toSimpleSchema(data)
    } return null
  },
  fields() {
    const { name } = Template.parentData()
    const data = DynamicForms.findOne({ name })
    if (data) {
      return _.map(data.form, x => _.extend(x, { field: x.name }))
    }
  },
  action() {
    return function(els, callbacks, changed) {
      /* console.log('[forms] Action running!') */
      /* console.log('[forms] Form data!', this) */
      /* console.log('[forms] HTML elements with `.reactive-element` class!', els) */
      /* console.log('[forms] Callbacks!', callbacks) */
      /* console.log('[forms] Changed fields!', changed) */

      callbacks.success() // Display success message.
      callbacks.reset() // Run each Element's custom `reset` function to clear the form.
    }
  },
})
