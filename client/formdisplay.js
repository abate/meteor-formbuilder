import { moment } from 'meteor/momentjs:moment'
import { toSimpleSchema } from '../both/helpers'

Template.formBuilderDisplay.onCreated(function onCreated() {
  const template = this
  template.subscribe('FormBuilder.dynamicForms')
})

Template.formBuilderDisplay.helpers({
  fieldTemplate(type) {
    switch (type) {
      case 'select': return 'basicFormDisplaySelect'
      case 'checkbox-group': return 'basicFormDisplayInputGroupCheckBox'
      case 'radio-group': return 'basicFormDisplayInputGroupRadio'
      case 'textarea': return 'basicFormDisplayTextarea'
      case 'date': return 'basicFormDisplayDate'
      default: return 'basicFormDisplayInput'
    }
  },
  equal(a, b) { return (a === b) },
  schema() {
    const name = Template.currentData().formName
    const form = DynamicForms.findOne({ name })
    if (form) {
      return toSimpleSchema(form)
    } return null
  },
  fields() {
    const name = Template.currentData().formName
    const form = Template.currentData().form
    const data = DynamicForms.findOne({ name })
    if (data && form) {
      return _.map(data.form, x => _.extend(x, { field: x.name, value: form[x.name] }))
    } return null
  },
})

Template.basicFormDisplayInputGroupCheckBox.helpers({
  atLeastOne(values) {
    return _.some(values, v => (v.hasOwnProperty('selected')))
  },
})

Template.basicFormDisplayInputGroupRadio.helpers({
  checked(opt, value) {
    if (value instanceof Array) {
      return _.find(value, v => (v === opt.value))
    }
    return (value === opt.value)
  },
})

Template.basicFormDisplayDate.helpers({
  formatDate(value) {
    return moment(value).format('MMMM Do YYYY')
  },
})
